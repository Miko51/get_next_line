[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Get Next Line

**Bu projede dosyadan satır satır okuma işlemi yapan get_next_line fonksiyonu yapılır. Posix fonksiyonları kullanılır ve temel amaçlarından birisi static veri tipinin öğrenilmesidir.**

## Kullanım

Proje klonlamanız yeterlidir. Bonus ve ana kısıma ait dört tane dosya bulunmaktadır. Bonus ile birden fazla dosyayı okuyabilirsiniz, ana kısım ise sadece tek dosya okur. Fonksiyon prototipi şu şekildedir: char \*get_next_line(int fd). Parametre file description değerini almaktadır.

## Not

Proje notu %125. [Projenin detaylı yönergesi](en.subject.pdf)
