[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Get Next Line

**In this project, the get_next_line function is implemented to read line by line from a file. POSIX functions are used, and one of the primary objectives is to learn about the static data type.**

## Usage 

Cloning the project is sufficient. There are four files belonging to both the bonus and the main part. With the bonus, you can read multiple files, whereas the main part reads only one file. The function prototype is as follows: char *get_next_line(int fd). It takes the file description parameter.

## Score

125/125. [Project subject](en.subject.pdf)
