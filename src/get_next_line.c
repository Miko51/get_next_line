/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/27 16:56:13 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 13:32:25 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/get_next_line.h"

char	*output(char *str)
{
	char	*temp;
	size_t	i;

	i = 0;
	temp = ft_calloc(ft_strlen(str) + 1, 1);
	if (!temp)
		return (NULL);
	while (str && str[i] != '\0')
	{
		temp[i] = str[i];
		if (str[i] == '\n')
			break ;
		i++;
	}
	temp[ft_strlen(str)] = '\0';
	return (temp);
}

char	*pass(char *str)
{
	char	*p;
	int		i;
	int		j;

	j = 0;
	i = 0;
	while (str[i] != '\n' && str[i])
		i++;
	if (!str[i] || (str[i] == '\n' && !str[i + 1]))
	{
		free(str);
		return (NULL);
	}
	i++;
	p = (char *) ft_calloc(ft_strlen(str) - i + 1, 1);
	while (str[i])
		p[j++] = str[i++];
	p[j] = '\0';
	free(str);
	return (p);
}

char	*my_read2(char *buff, int fd)
{
	char	*temp;
	int		len;

	temp = (char *) ft_calloc((BUFFER_SIZE + 1), 1);
	while (!ft_strchr(buff, '\n'))
	{
		len = read(fd, temp, BUFFER_SIZE);
		temp[len] = '\0';
		if (len <= 0 && !ft_strlen(buff))
		{
			free(temp);
			return (NULL);
		}
		else if (len <= 0 && ft_strlen(buff))
			break ;
		buff = ft_strjoin(buff, temp);
	}
	free(temp);
	return (buff);
}

char	*get_next_line(int fd)
{
	static char	*buff;
	char		*temp;

	temp = NULL;
	if (fd < 0)
		return (NULL);
	if ((buff && *buff) && ft_strchr(buff, '\n'))
	{
		temp = output(buff);
		buff = pass(buff);
		return (temp);
	}
	else
	{
		buff = my_read2(buff, fd);
		if (!buff)
			return (0);
		temp = output(buff);
		buff = pass(buff);
		return (temp);
	}
}
