/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/27 17:00:15 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 13:33:06 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "../include/get_next_line.h"

void	ft_bzero(void *s, size_t n)
{
	while (n > 0)
	{
		*((unsigned char *)s) = (unsigned char) NULL;
		s++;
		n--;
	}
}

void	*ft_calloc(size_t num, size_t size)
{
	void	*mem;

	mem = malloc(num * size);
	if (!mem)
		return (NULL);
	ft_bzero(mem, num * size);
	return (mem);
}

char	*ft_strchr(const char *str, int c)
{
	if (!ft_strlen((char *)str))
		return (0);
	while (*str)
	{
		if (*str == (unsigned char)c)
		{
			return ((char *)str);
		}
		str++;
	}
	if (!c)
		return ((char *)str);
	return (NULL);
}

size_t	ft_strlen(char *chr)
{
	size_t	i;

	i = 0;
	if (!chr)
		return (0);
	while (chr[i])
		i++;
	return (i);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*temp;
	size_t	len;
	size_t	k;
	size_t	i;

	k = 0;
	i = ft_strlen((char *) s1);
	len = ft_strlen((char *) s1) + ft_strlen((char *) s2);
	temp = (char *) ft_calloc(len + 1, 1);
	if (!temp)
		return (NULL);
	while (i > k)
		*temp++ = s1[k++];
	k = 0;
	i = ft_strlen((char *) s2);
	while (i > k)
		*temp++ = s2[k++];
	if (ft_strlen((char *) s1))
	{
		free((char *) s1);
		s1 = NULL;
	}
	*(temp) = '\0';
	return (temp - len);
}
